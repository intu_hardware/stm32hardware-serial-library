
#include "StmSer.h"

static uint8_t StmSerPortsCount = 0;

StmSer::StmSer() {
	this->RxTxDefined = false;
	if (StmSerPortsCount>SERIAL_PORT_HARDWARE_COUNT) {
		this->myType = 1;
	}
	else {
		this->myType = 0;
	}
	this->myNumber = StmSerPortsCount;
	StmSerPortsCount ++;
}

void StmSer::begin (long speed) {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			Serial.begin (speed);
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			Serial1.begin (speed);
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			Serial2.begin (speed);
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			Serial3.begin (speed);
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				this->mySerial->begin(speed);
			}
			break;
#endif
	}
}

size_t StmSer::write(uint8_t byte) {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			return Serial.write(byte);
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			return Serial1.write(byte);
			Serial1.write(speed);
			Serial1.setRxTxPins()
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			return Serial2.write(byte);
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			return Serial3.write(byte);
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				return this->mySerial->write(byte);
			}
			break;
#endif
	}
	return 0;
}

int StmSer::read() {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			return Serial.read();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			return Serial1.read();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			return Serial2.read();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			return Serial3.read();
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				return this->mySerial->read();
			}
			break;
#endif
	}
	return 0;
}

int StmSer::available() {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			return Serial.available();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			return Serial1.available();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			return Serial2.available();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			return Serial3.available();
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				return this->mySerial->available();
			}
			break;
#endif
	}
	return 0;
}

void StmSer::flush() {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			Serial.flush();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			Serial1.flush();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			Serial2.flush();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			Serial3.flush();
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				this->mySerial->flush();
			}
			break;
#endif
	}
}

int StmSer::peek() {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			return Serial.peek();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			return Serial1.peek();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			return Serial2.peek();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			return Serial3.peek();
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				return this->mySerial->peek();
			}
			break;
#endif
	}
	return 0;
}

bool StmSer::listen() {
#if defined(ARDUINO_ARCH_AVR)
	if (this->myType==1) {
		if (this->RxTxDefined) {
			return this->mySerial->listen();
		}
		return false;
	}
#else
	if (this->myType==1) {
		return false;
	}
#endif
	return true;
}

void StmSer::end() {
	switch (this->myType) {
	case 0:
		switch (this->myNumber) {
#ifdef SERIAL_PORT_HARDWARE
		case 0:
			Serial.end();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE1
		case 1:
			Serial1.end();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE2
		case 2:
			Serial2.end();
			break;
#endif
#ifdef SERIAL_PORT_HARDWARE3
		case 3:
			return Serial3.end();
			break;
#endif
		default:
			break;
		}
		break;
#if defined(ARDUINO_ARCH_AVR)
		case 1:
			if (this->RxTxDefined) {
				this->mySerial->end();
			}
			break;
#endif
	}
}
bool StmSer::isListening() {
#if defined(ARDUINO_ARCH_AVR)
	if (this->myType==1) {
		if (this->RxTxDefined) {
			return this->mySerial->isListening();
		}
		return false;
	}
#else
	if (this->myType==1) {
		return false;
	}
#endif
	return true;
}
bool StmSer::stopListening() {
#if defined(ARDUINO_ARCH_AVR)
	if (this->myType==1) {
		if (this->RxTxDefined) {
			return this->mySerial->stopListening();
		}
		return true;
	}
#else
	if (this->myType==1) {
		return true;
	}
#endif
	return false;
}
bool StmSer::overflow() {
#if defined(ARDUINO_ARCH_AVR)
	if (this->myType==1) {
		if (this->RxTxDefined) {
			return this->mySerial->overflow();
		}
	}
#endif
	return false;
}

StmSer::~StmSer()
{
#if defined(ARDUINO_ARCH_AVR)
	if (this->RxTxDefined) {
		this->mySerial->~SoftwareSerial();
	}
#endif
	StmSerPortsCount--;
}

bool StmSer::setRxTxPins (uint8_t Rx,uint8_t Tx) {
#if defined(ARDUINO_ARCH_AVR)
	if (this->myType==1) {
		if (this->RxTxDefined) {
			this->mySerial->~SoftwareSerial();
		}
		this->mySerial = new SoftwareSerial(Rx,Tx);
		this->RxTxDefined = true;
		return true;
	}
#endif
	return false;
}

bool StmSer::isHardwareSerial() {
	if (this->myType==0) return true;
	return false;
}

uint8_t StmSer::getSerialNumber() {
	return this->myNumber;
}
